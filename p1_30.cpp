#include <iostream>
#include "p1_30.h"

f3::f3(){
    root = NULL;
    for (auto & i : min) i = NULL;
    minLength = 0;
    //cout<<"Structure created"<<endl;
}

void f3::push(int i){
    if (root == NULL) {
        root = new node(i, NULL, NULL);
        min[0] = root;
        minLength = 1;
        //cout<<" created new root of value "<<root->value<<" | smallest - "<<min[0]->value<<" |";
    } else {
        node* newroot = new node(i, root, NULL);
        root->next = newroot;
        root = newroot;
        if (min[minLength-1]->value > i) {
            min[minLength] = root;
            minLength++;
            //cout<<" | new smallest |";
        }
        //cout<<" added new root of value "<<root->value;
    }
}

void f3::pop(){
    if (root != NULL) {
        node* oldroot = root->prev;
        int oldval = -1;
        if (oldroot != NULL) {
            oldroot->next = NULL;
            oldval = oldroot->value;
        }
        //cout<<root->value<<" removed, "<<oldval<<" is the new root";
        if (min[minLength-1] == root) {
            min[minLength-1] = NULL;
            minLength--;
        }
        root = oldroot;
    } else {
        //cout<<"nothing happened";
    }
}

void f3::uptomin(){
    if (root != NULL) {
        node* newroot = min[minLength-1]->prev;
        if (newroot != NULL) newroot->next = NULL;
        min[minLength-1] = NULL;
        minLength--;
        root = newroot;
        int newval = -1;
        if (root != NULL) newval = root->value;
        //cout<<newval<<" is the new root";
    } else {
        //cout<<"nothing happened";
    }
}

void f3::display(){
    node* i = root;
    std::cout<<"Structure: ";
    while (i != NULL) {
        std::cout<<" "<<i->value;
        i = i->prev;
    }
    std::cout<<std::endl;
}
