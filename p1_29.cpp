#include <iostream>
#include "p1_29.h"

f2::f2(){
    root = NULL;
    for (auto & i : tab) i = NULL;
}

void f2::push(int i){
    if (root == NULL) {
        root = new node(i, NULL, NULL);
        tab[i] = new pointerNode(root, NULL);
        //cout<<" created root of value "<<root->value;
    } else {
        node* newroot = new node(i, root, NULL);
        root->next = newroot;
        root = newroot;
        if (tab[i] != NULL) {
            tab[i] = new pointerNode(root, tab[i]);
        } else {
            tab[i] = new pointerNode(root, NULL);
        }
        //cout<<" added value "<<root->value;
    }
}

void f2::pop(){
    if (root != NULL) {
        node* oldroot = root->prev;
        int oldval = -1;
        if (oldroot != NULL) {
            oldroot->next = NULL;
            oldval = oldroot->value;
        }
        if (tab[root->value]->prev != NULL) {
            tab[root->value] = tab[root->value]->prev;
        } else {
            tab[root->value] = NULL;
        }
        //cout<<root->value<<" removed, "<<oldval<<" is the new root";
        root = oldroot;
    } else {
        //cout<<"nothing happened";
    }
}

bool f2::search(int i){
    if (i<N && tab[i]!=NULL) return true;
    else return false;
}

void f2::ddelete(int i){
    if (search(i)) {
        if (root->value == i) {
            pop();
        } else {
            if (tab[i]->value->prev == NULL) {
                if (tab[i]->value->next != NULL) {
                    tab[i]->value->next->prev = NULL;
                }
            } else {
                tab[i]->value->next->prev = tab[i]->value->prev;
                tab[i]->value->prev->next = tab[i]->value->next;
            }
            if (tab[i]->prev != NULL) {
                tab[i] = tab[i]->prev;
            } else {
                tab[i] = NULL;
            }
        }
        //cout<<" successfully deleted "<<i;
    } else {
        //cout<<" nothing happened";
    }
}

void f2::display(){
    node* i = root;
    std::cout<<"Structure: ";
    while (i != NULL) {
        std::cout<<" "<<i->value;
        i = i->prev;
    }
    std::cout<<std::endl;
}
