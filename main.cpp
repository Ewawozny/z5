#include <iostream>
#include "N.h"
#include "p1_28.h"
#include "p1_29.h"
#include "p1_30.h"

using namespace std;

// 1.28 1.29 1.30

int main() {

    // rozwiazania znajduja sie w plikach pod odpowiednia nazwa
    // ponizej krotkie testy poszczegolnych funkcji:
    cout<<"1.28"<<endl;
        f1 root;
    cout<<"Insert 1"<<endl; root.insert(1);
    cout<<"Insert 0"<<endl; root.insert(0);
    cout<<"Insert 10"<<endl; root.insert(10);
    
    cout<<"Search 5: "<<root.search(5)<<endl;
    cout<<"Search 10: "<<root.search(10)<<endl;
    cout<<"Search 25: "<<root.search(25)<<endl;
    cout<<"Search 0: "<<root.search(0)<<endl;
    
	cout<<"Select"<<endl; root.select();
	
	cout<<"Search 1: "<<root.search(1)<<endl;
	cout<<"Search 0: "<<root.search(0)<<endl;
    cout<<"Search 10: "<<root.search(10)<<endl;

    cout<<endl<<"1.29"<<endl;
    	f2 root2;
	cout<<"Push 10: "; root2.push(10); cout<<endl;
    cout<<"Push 2: "; root2.push(2); cout<<endl;
    cout<<"Push 1: "; root2.push(1); cout<<endl;
    cout<<"Push 3: "; root2.push(3); cout<<endl;
    cout<<"Push 10: "; root2.push(10); cout<<endl;
    root2.display();
    
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    
    cout<<"Push 0: "; root2.push(0); cout<<endl;
    cout<<"Push 10: "; root2.push(10); cout<<endl;
    cout<<"Push 10: "; root2.push(10); cout<<endl;
    cout<<"Push 5: "; root2.push(5); cout<<endl;
    cout<<"Push 25: "; root2.push(25); cout<<endl;
    cout<<"Push 10: "; root2.push(10); cout<<endl;
    cout<<"Push 1: "; root2.push(1); cout<<endl;
    cout<<"Push 10: "; root2.push(10); cout<<endl;
    root2.display();
    
    cout<<"Search 100: "<<root2.search(100)<<endl;
    cout<<"Delete 100: "; root2.ddelete(100); cout<<endl;
    cout<<"Search 10: "<<root2.search(10)<<endl;
    root2.display();
    
    cout<<"Delete 10: "; root2.ddelete(10); cout<<endl;
    root2.display();
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    cout<<"Pop: "; root2.pop(); cout<<endl;
    root2.display();
    cout<<"Search 10: "<<root2.search(10)<<endl;
    cout<<"Delete 10: "; root2.ddelete(10); cout<<endl;
    root2.display();
    cout<<"Delete 10: "; root2.ddelete(10); cout<<endl;
    root2.display();
    cout<<"Delete 10: "; root2.ddelete(10); cout<<endl;
    root2.display();
    cout<<"Delete 10: "; root2.ddelete(10); cout<<endl;
    root2.display();
    
    cout<<endl<<"1.30"<<endl;
    	f3 root3;
    cout<<"Pop: "; root3.pop(); cout<<endl;
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    
    cout<<"Push 0: "; root3.push(0); cout<<endl;
    root3.display();
    cout<<"Pop: "; root3.pop(); cout<<endl;
    root3.display();
    
    cout<<"Push 0: "; root3.push(0); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    
    cout<<"Push 6: "; root3.push(6); cout<<endl;
    cout<<"Push 10: "; root3.push(10); cout<<endl;
    cout<<"Push 5: "; root3.push(5); cout<<endl;
    cout<<"Push 25: "; root3.push(25); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    //cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    cout<<"Pop: "; root3.pop(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Push 6: "; root3.push(6); cout<<endl;
    cout<<"Push 10: "; root3.push(10); cout<<endl;
    cout<<"Push 5: "; root3.push(5); cout<<endl;
    cout<<"Push 25: "; root3.push(25); cout<<endl;
    cout<<"Push 6: "; root3.push(6); cout<<endl;
    cout<<"Push 10: "; root3.push(10); cout<<endl;
    cout<<"Push 5: "; root3.push(5); cout<<endl;
    cout<<"Push 1: "; root3.push(1); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    
    cout<<"Push 6: "; root3.push(6); cout<<endl;
    cout<<"Push 5: "; root3.push(5); cout<<endl;
    cout<<"Push 4: "; root3.push(4); cout<<endl;
    cout<<"Push 3: "; root3.push(3); cout<<endl;
    cout<<"Push 2: "; root3.push(2); cout<<endl;
    cout<<"Push 1: "; root3.push(1); cout<<endl;
    cout<<"Push 1: "; root3.push(1); cout<<endl;
    cout<<"Push 1: "; root3.push(1); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    cout<<"Uptomin: "; root3.uptomin(); cout<<endl;
    root3.display();
    
    return 0;
}
