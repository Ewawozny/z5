#pragma once

#ifndef p1_29_h
#define p1_29_h
#include "N.h"

class f2 {  // 1.29
    struct node {
        node* prev;
        node* next;
        int value;

        node(int v, node* pre, node* nex){
            value = v;
            prev = pre;
            next = nex;
        }
    };

    struct pointerNode {
        pointerNode* prev;
        node* value;

        pointerNode(node* v, pointerNode* pre){
            value = v;
            prev = pre;
        }
    };

    node* root;
    pointerNode* tab[N];

public:
    f2();
    void push(int);
    void pop();
    bool search(int);
    void ddelete(int);
    void display();
};

#endif
