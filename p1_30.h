#pragma once

#ifndef p1_30_h
#define p1_30_h
#include "N.h"

class f3 {  // 1.30
    struct node {
        node* prev;
        node* next;
        int value;

        node(int v, node* pre, node* nex){
            value = v;
            prev = pre;
            next = nex;
        }
    };

    node* root;
    node* min[N];
    int minLength;

public:
    f3();
    void push(int);
    void uptomin();
    void pop();
    void display();
};

#endif
