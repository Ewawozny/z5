#include <iostream>
#include <random>
#include "p1_28.h"

f1::f1(){
    for (int & i : a){
        i = 0;
    }
}

void f1::select(){
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(0, (N-1));
    a[distr(gen)] = 0;
}

bool f1::search(int i){
    if (i<N && a[i] == 1) return true;
    else return false;
}

void f1::insert(int i){
    if (i<N) a[i] = 1;
}
