#pragma once

#ifndef f1_h
#define f1_h
#include "N.h"

class f1 {  // 1.28
public:
    int a[N];

    f1();
    void select();
    bool search(int);
    void insert(int);
};

#endif